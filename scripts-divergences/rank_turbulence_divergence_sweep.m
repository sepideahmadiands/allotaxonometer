function [divergences,normalizations] = rank_turbulence_divergence_sweep(mixedelements,alphavals,Nvals)
%% [divergences,normalizations] = rank_turbulence_divergence_sweep(mixedelements,alphavals,Nvals)
%% 
%% returns partial sum divergence values for rank divergence for
%% each alpha in alphavals and each N in Nvals
%% 
%% divergences = length(alphavals) * length(Nvals)
%% 
%% alpha >= 0 and may be specified as Inf
%% 
%% starts with
%% normalization by a (alpha+1)/alpha prefactor and using a power 1/(alpha+1)
%% 

for alpha_i=1:length(alphavals)
    alpha = alphavals(alpha_i);
    
    [deltas,normalization] = rank_turbulence_divergence(mixedelements,alpha);

    [deltas_sorted,indices] = sort(deltas,'descend');

    %% compute partial sums
    for N_i=1:length(Nvals)
        N = Nvals(N_i);
        if (N > length(deltas))
            N = length(deltas);
        end
        divergences(alpha_i,N_i) = sum(deltas_sorted(1:N));
        normalizations(alpha_i,N_i) = normalization;
    end
end
