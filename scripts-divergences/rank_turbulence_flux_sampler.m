function rankfluxes = rank_turbulence_flux_sampler(mixedelements,ranks)
%% 
%% rankfluxes = rank_turbulence_flux_sampler(mixedelements,ranks)
%% 
%% returns 
%% and indices of elements moving below rank _rank_

%% 1/2. upwards flux:

for i=1:length(ranks)
    rankfluxes(i,1) = sum( ...
        (mixedelements(1).ranks > ranks(i)) ...
        & ...
        (mixedelements(2).ranks <= ranks(i)));
end
