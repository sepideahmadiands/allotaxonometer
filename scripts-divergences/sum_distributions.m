function elements_summed = sum_distributions(elements)
%% elements_summed = sum_distributions(elements)
%% 
%% expects input of the form of a vector of structures, length n
%% 
%% two pieces are required, i=1:n:
%% 
%% elements(i).types 
%% - cell array
%% - names of things (types, elements, species, ...)
%% - single column
%% 
%% elements(i).sizes
%% - sizes of things (number, points, dollars, ...)
%% - may be a matrix; column per attribute
%% 
%% produces:
%% 
%% elements_summed.types (cell array, names of things, union)
%% elements_summed.sizes (sum of sizes over systems for each type)
%% 
%% elements_summed.ranks (ranks, tied, based on counts with 0s)


%% create overall type list, merging one system in at at time
elements_summed(1).types = elements(1).types;
for i=2:length(elements)
    elements_summed(1).types = ...
        union(elements_summed(1).types, ...
              elements(i).types, ...
              'stable');
end

N = length(elements_summed(1).types);
elements_summed(1).counts = zeros(N,1);

%% go through systems, and add in counts for types present in that system
for i=1:length(elements)
    [presence,indices] = ...
        ismember(elements_summed(1).types,...
                 elements(i).types);
    newindices = find(presence==1);

    elements_summed(i).counts(newindices) = ...
        elements_summed(i).counts(newindices) + elements(i).counts(indices(newindices));
end

%% sort by counts, descending
[~,indices] = sort(elements_summed(1).counts,'descend');
elements_summed(1).types = elements_summed(1).types(indices);
elements_summed(1).counts = elements_summed(1).counts(indices);

%% compute ranks
elements_summed(1).ranks = tiedrank(-elements_summed(1).counts);


