function [divergence,divergence_elements] = sorensen(p1,p2)
%% [divergence,divergence_elements] = sorensen(p1,p2)
%% 
%% If p_1 and p_2 are normlized, returns 1/2 L1 norm
%% 
%% Normalization is the sum of p_1 + p_2 so can be partial
%% 
%% S{\o}rensen, T. (1948) A method of establishing groups of equal
%% amplitude in plant sociology based on similarity of species
%% and its application to analyses of the vegetation on Danish
%% commons. Biologiske Skrifter / 
%% Kongelige Danske Videnskabernes Selskab, 5 (4): 1--34.
%% 
%% Looman, J. and Campbell, J.B. (1960) Adaptation of
%% S{\o}rensen's K (1948) for estimating unit affinities in
%% prairie vegetation. Ecology 41: 409--416
%% 
%% 
%% p1 and p2 must be of the same length and correspond to the same
%% elements

divergence_elements = abs(p1 - p2)./sum(p1 + p2);

divergence = sum(divergence_elements);
