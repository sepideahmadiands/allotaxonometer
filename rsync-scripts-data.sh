## general tools

rsync -av ~/matlab/general/print_universal.m \
      scripts-general/

rsync -av ~/matlab/general/cheese_error001.m \
      scripts-general/

## divergence tools

rsync -av ~/matlab/general/alpha_norm_type2.m \
      scripts-divergences/

rsync -av ~/matlab/general/alpha_divergence_symmetric_type2.m \
      scripts-divergences/

## figure makers

rsync -av ~/work/stories/2018-06story-turbulence/figures/figzipfshuffling004.m \
      scripts-figures/

rsync -av ~/work/stories/2018-06story-turbulence/figures/figzipfshuffling004.m \
      scripts-figures/


